package zkafka

import (
	"context"
	"encoding/json"
	"fmt"
	"testing"
	"time"

	"github.com/segmentio/kafka-go"
)

type Message struct {
	ChgValue     int    `json:"chg_value"`
	CreateMsTime int64  `json:"create_ms_time"`
	CreateTime   int    `json:"create_time"`
	Desc         string `json:"desc"`
	GroupId      int    `json:"group_id"`
	MapId        int    `json:"map_id"`
	NewValue     int    `json:"new_value"`
	OldValue     int    `json:"old_value"`
	OrderId      int    `json:"order_id"`
	Session      string `json:"session"`
	Type         int    `json:"type"`
	UserId       int64  `json:"user_id"`
}

func TestNewReader(t *testing.T) {
	k := NewReader()
	fmt.Println("初始化完成")
	for true {
		if msg, err := k.ReadMessage(context.TODO()); err == nil {
			m := Message{}
			_ = json.Unmarshal(msg.Value, &m)
			fmt.Println(m)
			// if m.Desc == "loc-sjz-test" {
			// }
		} else {
			fmt.Println("err", err.Error())
		}
	}
}

func TestNewWriter(t *testing.T) {
	k := NewWriter()
	fmt.Println("初始化完成")
	err := k.WriteMessages(context.TODO(), kafka.Message{
		Value: []byte("{\"chg_value\":30,\"create_ms_time\":1685465380202,\"create_time\":1685465380,\"desc\":\"loc-sjz-test\",\"group_id\":0,\"map_id\":0,\"new_value\":30,\"old_value\":0,\"order_id\":1685465380,\"session\":\"\",\"type\":9999,\"user_id\":4198430797}"),
		Time:  time.Time{},
	})
	if err != nil {
		fmt.Println("err", err.Error())
	}
}
