package zredis

import (
	"context"

	"github.com/redis/go-redis/v9"
)

type Config struct {
	Addr     string `json:"addr"`
	Password string `json:"password"`
	DB       int    `json:"db"`
}

func New(config Config) *redis.Client {
	cli := redis.NewClient(&redis.Options{
		Addr:     config.Addr,
		Password: config.Password, // 没有密码，默认值
		DB:       config.DB,       // 默认DB 0
	})

	cli.Ping(context.TODO())
	return cli
}
