package libs

import (
	"os"
)

// DirFile 目录下文件
func DirFile(dirname string) ([]os.FileInfo, error) {
	dir, err := os.Open(dirname)
	if err != nil {
		return nil, err
	}

	list, err := dir.Readdir(-1)
	_ = dir.Close()
	if err != nil {
		return nil, err
	}
	return list, nil
}
