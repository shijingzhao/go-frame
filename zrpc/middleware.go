package zrpc

// MiddlewareRoutes 注册带中间件的路由
func MiddlewareRoutes(middlewares []Middleware, handlers []Handler) []Handler {
	l := len(middlewares)
	for x := l - 1; x >= 0; x-- {
		for y := range handlers {
			handlers[y] = Handler{
				Name:     handlers[y].Name,
				Obj:      handlers[y].Obj,
				Input:    handlers[y].Input,
				Output:   handlers[y].Output,
				Function: handlers[y].Function,
			}
		}
	}
	return handlers
}
