package zhttp

// MiddlewareRoutes 注册带中间件的路由
func MiddlewareRoutes(middlewares []Middleware, routes []Route) []Route {
	l := len(middlewares)
	for x := l - 1; x >= 0; x-- {
		for y := range routes {
			routes[y] = Route{
				Method:  routes[y].Method,
				Path:    routes[y].Path,
				Handler: middlewares[x](routes[y].Handler),
			}
		}
	}
	return routes
}
