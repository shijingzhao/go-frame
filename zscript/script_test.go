package zscript

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"testing"
	"time"
)

func TestNewServer(t *testing.T) {
	ctx, cancel := context.WithCancel(
		context.Background(),
	)

	wg := &sync.WaitGroup{}
	ser := New(ctx, Config{})
	ser.AddHandler([]Handler{
		{Name: "ScriptHello1", UserName: "SJZ", Arg: ScriptArgHello1{A: 10}, Env: nil, Function: ScriptHello1},
		{Name: "ScriptHello2", UserName: "SJZ", Arg: ScriptArgHello2{A: 20}, Env: nil, Function: ScriptHello2},
	})

	wg.Add(1)
	registerExitListening(func() {
		defer wg.Done()

		cancel()
		fmt.Println("end")
	})

	ser.Start(wg)
	wg.Wait()
}

func registerExitListening(fun func()) {
	ch := make(chan os.Signal, 1)
	signal.Notify(
		ch,
		syscall.SIGTERM,
		syscall.SIGINT,
	)

	go func() {
		_ = <-ch
		fun()
	}()
}

type ScriptArgHello1 struct {
	A int
}
type ScriptArgHello2 struct {
	A int
}

func ScriptHello1(ctx context.Context, arg any) {
	fmt.Println("ScriptHello1", arg.(ScriptArgHello1).A)
	for {
		if StopSignal(ctx) {
			return
		}

		fmt.Println(time.Now().Format("2006-01-02 15:04:05.000000"), "ScriptHello1")
		time.Sleep(1 * time.Second)
	}
}

func ScriptHello2(ctx context.Context, arg any) {
	fmt.Println("ScriptHello2", arg.(ScriptArgHello2).A)
	for {
		if StopSignal(ctx) {
			return
		}

		fmt.Println(time.Now().Format("2006-01-02 15:04:05.000000"), "ScriptHello2")
		time.Sleep(1 * time.Second)
	}
}
