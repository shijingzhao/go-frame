package core

import (
	"encoding/json"
	"os"

	"gitee.com/shijingzhao/go-frame/zcrontab"
	"gitee.com/shijingzhao/go-frame/zhttp"
	"gitee.com/shijingzhao/go-frame/zlog"
	"gitee.com/shijingzhao/go-frame/zrpc"
	"gitee.com/shijingzhao/go-frame/zscript"
)

// AppConfigFile 配置文件名
const AppConfigFile = "config/app.json"

var AppConfig struct {
	App Config      `json:"app"`
	Log zlog.Config `json:"log"` // 日志目录

	RpcSer  zrpc.SerConfig  `json:"rpc_ser"`  // rpc端口
	HttpSer zhttp.SerConfig `json:"http_ser"` // http端口

	ScriptSer  zscript.Config  `json:"script_ser"`  // script配置
	CrontabSer zcrontab.Config `json:"crontab_ser"` // crontab配置
}

// ParseJsonConfigFile 解析配置文件
func ParseJsonConfigFile(file string, config any) error {
	content, err := os.ReadFile(file)
	if err != nil {
		return err
	}

	return json.Unmarshal(content, config)
}
