package zrpc

import (
	"context"
	"net"
	"net/rpc"
)

type SerConfig struct {
	Addr string `json:"addr"`
}

type CliConfig struct {
	Addr   string `json:"addr"`
	Method string `json:"method"`
}

// Middleware 中间件
type Middleware func(next Handler) Handler

type Handler struct {
	Name     string
	Obj      any
	Input    any
	Output   any
	Function func(ctx context.Context, args, reply any) error
}

type Server struct {
	ctx context.Context

	config SerConfig

	ser *rpc.Server
	lis *net.TCPListener

	Handlers []Handler
}

type Client struct {
	ctx context.Context

	config CliConfig

	client *rpc.Client
}
