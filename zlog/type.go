package zlog

import (
	"context"
	"io"
)

const (
	colon     = ":"
	slash     = "/"
	wrap      = "\n"
	splicing  = " "
	separator = "="
)

const (
	configWayLocal   = "local"   // 本地
	configWayRemote  = "remote"  // 远程
	configWayConsole = "console" // 控制台
)

type Config struct {
	Dir string `json:"dir"` // 目录
	Way string `json:"way"` // 方式 "local|remote|console"
}

type Factory interface {
	SetSn() Factory
	SetFix() Factory
	SetFile(name string) Factory

	Info(msg string, fields ...Field)
	Debug(msg string, fields ...Field)
	Error(msg string, fields ...Field)
	Sql(msg string, fields ...Field)
}

type Logger struct {
	ctx context.Context

	name   string
	sn     string
	prefix string
	suffix string
}

// LocalLogger 本地日志
type LocalLogger struct {
	Logger
	writer io.Writer
}

// RemoteLogger 远程日志
type RemoteLogger struct {
	Logger
	writer io.Writer
}

// ConsoleLogger 远程日志
type ConsoleLogger struct {
	Logger
	writer io.Writer
}
