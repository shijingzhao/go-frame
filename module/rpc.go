package module

import (
	"gitee.com/shijingzhao/go-frame/module/pb"
)

type Airth struct{}

func (a *Airth) ArithAddHandlerRpc(rq *pb.ArithRequest, rs *pb.ArithResponse) error {
	rs.C = rq.A + rq.B
	return nil
}

func (a *Airth) ArithSubHandlerRpc(rq *pb.ArithRequest, rs *pb.ArithResponse) error {
	rs.C = rq.A - rq.B
	return nil
}
