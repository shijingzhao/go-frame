package module

import (
	"fmt"
	"net/http"

	"gitee.com/shijingzhao/go-frame/zhttp"
)

type Post struct {
	Title   string `json:"title"`
	Content int    `json:"content"`
}

func Hello1(w http.ResponseWriter, r *http.Request) {
	post := Post{}
	zhttp.JsonAnalysis(r, &post)
	zhttp.OutputJson(w, zhttp.Body{Code: 200, Msg: "success", Data: post})
}
func Hello2(w http.ResponseWriter, _ *http.Request) {
	fmt.Println("Hello2")
	_, _ = w.Write([]byte("Hello2"))
}
func MiddlewareHello1(next zhttp.HandlerFunc) zhttp.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("MiddlewareHello1")
		next(w, r)
	}
}
func MiddlewareHello2(next zhttp.HandlerFunc) zhttp.HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("MiddlewareHello2")
		next(w, r)
	}
}
