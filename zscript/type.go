package zscript

import "context"

type Config struct {
}

type Server struct {
	ctx context.Context

	config   Config
	handlers []Handler
}

// Handler 定义脚本执行的结构
type Handler struct {
	Name     string // 脚本名称
	UserName string // 脚本执行用户

	Arg      any      // 脚本参数
	Env      any      // 脚本环境变量
	Function Function // 脚本执行方法
}

type Function func(ctx context.Context, arg any)
