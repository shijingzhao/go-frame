package core

import (
	"os"
	"os/signal"
	"syscall"
)

func regExitListen(fun func()) {
	ch := make(chan os.Signal, 1)
	signal.Notify(ch, syscall.SIGTERM, syscall.SIGINT)

	go func() {
		_ = <-ch
		fun()
	}()
}
