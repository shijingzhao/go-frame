package zlog

import (
	"context"
	"testing"
)

func TestNew(t *testing.T) {
	logger := New(context.TODO()).SetSn()
	logger.SetSn().Info("Msg", Int("A", 65))
}

func TestNew2(t *testing.T) {
	Init(Config{
		Dir: "../log",
		Way: "local",
	})
	logger := New(context.TODO()).SetSn().SetFile("test")
	logger.SetSn().Info("Msg", Int("A", 65))

	for x := 0; x < 100; x++ {
		for y := 0; y < 100; y++ {
			go func(xv, yv int) {
				logger.SetSn().Info("Msg", Int("A", 65), Int("x", xv), Int("y", yv))
			}(x, y)
		}
	}
}

func TestNew3(t *testing.T) {
	Init(Config{
		Dir: "../log",
		Way: "con",
	})

	NewSystem(context.TODO()).Error("rpc服务器:终止错误", String("s.lis.Accept.err", "err.Error()"))
}
