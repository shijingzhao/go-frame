package zhttp

import (
	"context"
	"net/http"
	"net/url"
)

type SerConfig struct {
	Name string `json:"name"`
	Addr string `json:"addr"`
}

type Server struct {
	ctx    context.Context
	config SerConfig

	ser    *http.Server
	router *Router
}

type Client struct {
	url    string
	data   url.Values
	method string
	cli    *http.Client
	err    error
}

// Middleware 中间件
type Middleware func(next HandlerFunc) HandlerFunc

type Route struct {
	Method  string
	Path    string
	Handler HandlerFunc
}

type HandlerFunc func(http.ResponseWriter, *http.Request)

type Router struct {
	routes map[string]map[string]HandlerFunc
}
