package core

import (
	"flag"
)

var (
	ConfigPath string
)

func init() {
	flag.StringVar(&ConfigPath, "config", "./config/", "配置文件路径")

	flag.Parse()
}
