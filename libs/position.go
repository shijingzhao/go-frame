// Package libs 定位
package libs

import (
	"math"
)

// R 地球半径，单位米
const R = 6371000.0

// Rad 角度转化为弧度
const Rad = math.Pi / 180.0

type LatLng struct {
	Lng float64 // 经度
	Lat float64 // 纬度
}

func Distance(p1, p2 LatLng) float64 {
	lat1 := p1.Lat * Rad
	lng1 := p1.Lng * Rad
	lat2 := p2.Lat * Rad
	lng2 := p2.Lng * Rad
	theta := lng2 - lng1
	dist := math.Acos(math.Sin(lat1)*math.Sin(lat2) + math.Cos(lat1)*math.Cos(lat2)*math.Cos(theta))
	return dist * R
}
