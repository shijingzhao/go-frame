package zlog

import (
	"context"
	"fmt"
)

func newConsoleLogger(ctx context.Context) *ConsoleLogger {
	return &ConsoleLogger{
		Logger: Logger{
			ctx:    ctx,
			name:   "",
			sn:     "",
			prefix: "",
			suffix: "",
		},
	}
}

// SetSn 设置sn
func (l *ConsoleLogger) SetSn() Factory {
	l.sn = sn()
	return l
}

// SetFix 设置fix
func (l *ConsoleLogger) SetFix() Factory {
	return l
}

// SetFile 设置file
func (l *ConsoleLogger) SetFile(name string) Factory {
	l.writer = fileHandle(name)
	return l
}
func (l *ConsoleLogger) Info(msg string, fields ...Field) {
	fmt.Print(l.format("Info", msg, fields...))
}
func (l *ConsoleLogger) Debug(msg string, fields ...Field) {
	l.format("Debug", msg, fields...)
}
func (l *ConsoleLogger) Error(msg string, fields ...Field) {
	l.format("Error", msg, fields...)
}
func (l *ConsoleLogger) Sql(msg string, fields ...Field) {
	l.format("Sql", msg, fields...)
}
