package config

import (
	"fmt"

	"gitee.com/shijingzhao/go-frame/core"
	"gitee.com/shijingzhao/go-frame/zmysql"
	"gitee.com/shijingzhao/go-frame/zredis"
)

var ProjectConfig struct {
	Mysql zmysql.Config `json:"mysql"`
	Redis zredis.Config `json:"redis"`
}

func LoadConfig() {
	// 解析配置
	if err := core.ParseJsonConfigFile("config/project.json", &ProjectConfig); err != nil {
		panic(fmt.Sprintf("[配置文件错误] %s", err.Error()))
	}

	fmt.Println("ProjectConfig", ProjectConfig)
}
