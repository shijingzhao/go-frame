package zcrontab

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"sync"
	"syscall"
	"testing"
	"time"
)

func TestNew(t *testing.T) {
	ctx, cancel := context.WithCancel(
		context.Background(),
	)

	cron := New(ctx, Config{})
	cron.AddHandler([]Handler{
		{
			Name: "二进制",
			Spec: "1,55 20-26/2 12 * * *",
			Func: func() {
				fmt.Println("二进制", "Spec: 1,55 20-26/2 12 * * *", time.Now().Format("2006-01-02 15:04:05.000000"))
			},
		},
		{
			Name: "分钟",
			Spec: "0 */1 * * * * ",
			Func: func() {
				fmt.Println("分钟", "Spec: 0 */1 * * * *", time.Now().Format("2006-01-02 15:04:05.000000"))
			},
		},
		{
			Name: "秒",
			Spec: "*/1 * * * * * ",
			Func: func() {
				format := time.Now().Format("2006-01-02 15:04:05.000000")
				fmt.Println("秒", "Spec: */1 * * * * * str", format)
				time.Sleep(3 * time.Second)
				fmt.Println("秒", "Spec: */1 * * * * * end", format)
			},
		},
	})

	go func() {
		cron.Start()
	}()

	wg := sync.WaitGroup{}
	wg.Add(1)
	registerExitListening(func() {
		defer wg.Done()

		cron.Stop()

		cancel()
	})
	wg.Wait()
}

func registerExitListening(fun func()) {
	ch := make(chan os.Signal, 1)
	signal.Notify(
		ch,
		syscall.SIGTERM,
		syscall.SIGINT,
	)

	go func() {
		_ = <-ch
		fun()
	}()
}
