package zrpc

import (
	"context"
	"fmt"
	"net/rpc"
)

// NewClient 实例rpc客户端
func NewClient(ctx context.Context, config CliConfig) *Client {
	client, err := rpc.Dial("tcp", config.Addr)
	if err != nil {
		panic(any(fmt.Sprintf("rpc客户端:链接失败[%s]", err.Error())))
	}

	return &Client{
		ctx:    ctx,
		config: config,
		client: client,
	}
}

// Call 调用
func (c *Client) Call(args, reply any) error {
	return c.client.Call(c.config.Method, args, reply)
}
