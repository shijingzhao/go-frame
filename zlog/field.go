package zlog

import (
	"strconv"

	"gitee.com/shijingzhao/go-frame/utils"
)

type Field struct {
	Key string // 描述
	Val string // 说明
}

func Bool(key string, val bool) Field {
	return Field{Key: key, Val: strconv.FormatBool(val)}
}
func Int(key string, val int) Field {
	return Field{Key: key, Val: utils.IntToString(val)}
}
func Int8(key string, val int8) Field {
	return Field{Key: key, Val: utils.Int8ToString(val)}
}
func Int16(key string, val int16) Field {
	return Field{Key: key, Val: utils.Int16ToString(val)}
}
func Int32(key string, val int32) Field {
	return Field{Key: key, Val: utils.Int32ToString(val)}
}
func Int64(key string, val int64) Field {
	return Field{Key: key, Val: utils.Int64ToString(val)}
}

func Uint(key string, val uint) Field {
	return Field{Key: key, Val: utils.UintToString(val)}
}

func Uint8(key string, val uint8) Field {
	return Field{Key: key, Val: utils.Uint8ToString(val)}
}

func Uint16(key string, val uint16) Field {
	return Field{Key: key, Val: utils.Uint16ToString(val)}
}

func Uint32(key string, val uint32) Field {
	return Field{Key: key, Val: utils.Uint32ToString(val)}
}

func Uint64(key string, val uint64) Field {
	return Field{Key: key, Val: utils.Uint64ToString(val)}
}

func Float32(key string, val float32) Field {
	return Field{Key: key, Val: utils.Float32ToString(val)}
}
func Float64(key string, val float64) Field {
	return Field{Key: key, Val: utils.Float64ToString(val)}
}
func Byte(key string, val []byte) Field {
	return Field{Key: key, Val: utils.ByteToString(val)}
}

func String(key string, val string) Field {
	return Field{Key: key, Val: val}
}
