package zhttp

import (
	"encoding/json"
	"fmt"
	"net/http"
)

const (
	// ContentType means Content-Type.
	ContentType = "Content-Type"
	// ApplicationJson means application/json.
	ApplicationJson = "application/json"
)

type Body struct {
	Code int         `json:"code"`
	Msg  string      `json:"msg"`
	Data interface{} `json:"data,omitempty"`
}

func OutputJson(w http.ResponseWriter, body Body) {
	w.Header().Set(ContentType, ApplicationJson)

	j, err := json.Marshal(body)
	if err != nil {
		return
	}

	_, err = fmt.Fprintln(w, string(j))
	if err != nil {
		return
	}
}
