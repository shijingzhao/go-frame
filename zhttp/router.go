package zhttp

import (
	"net/http"
)

// Handle 添加路由
func (r *Router) Handle(route Route) {
	if r.routes == nil {
		r.routes = make(map[string]map[string]HandlerFunc)
	}
	if r.routes[route.Method] == nil {
		r.routes[route.Method] = make(map[string]HandlerFunc)
	}
	r.routes[route.Method][route.Path] = route.Handler
}

// 请求分发
func (r *Router) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	if h, ok := r.routes[req.Method][req.URL.Path]; ok {
		h(w, req)
	} else {
		OutputJson(w, Body{Code: 404, Msg: "not fount", Data: nil})
	}
}
