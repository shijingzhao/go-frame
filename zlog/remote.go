package zlog

import (
	"context"
)

func newRemoteLogger(ctx context.Context) *RemoteLogger {
	return &RemoteLogger{
		Logger: Logger{
			ctx:    ctx,
			name:   "",
			sn:     "",
			prefix: "",
			suffix: "",
		},
	}
}

// SetSn 设置sn
func (l *RemoteLogger) SetSn() Factory {
	l.sn = sn()
	return l
}

// SetFix 设置fix
func (l *RemoteLogger) SetFix() Factory {
	return l
}

// SetFile 设置file
func (l *RemoteLogger) SetFile(name string) Factory {
	return l
}
func (l *RemoteLogger) Info(msg string, fields ...Field) {

}
func (l *RemoteLogger) Debug(msg string, fields ...Field) {

}
func (l *RemoteLogger) Error(msg string, fields ...Field) {

}
func (l *RemoteLogger) Sql(msg string, fields ...Field) {

}
