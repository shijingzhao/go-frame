package reflex

import (
	"fmt"
	"testing"
)

type RQ struct {
	JsCode string `json:"js_code"`
	Jwt    string `json:"jwt"`
	Id     int8   `json:"id"`
	Is     bool   `json:"is"`
}

func TestMapToStruct(t *testing.T) {
	dest := RQ{}
	data := map[string]interface{}{
		"js_code": "1",
		"jwt":     "2",
		"id":      3,
		"is":      0,
	}

	if err := MapToStruct(data, &dest); err != nil {
		fmt.Println("err", err.Error())
	}

	fmt.Println("dest", dest)
}
