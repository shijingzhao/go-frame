package zmysql

import (
	"context"
	"fmt"
	"testing"
	"time"
)

type TUser struct {
	UserId    int64  `json:"user_id"`
	Account   string `json:"account"`
	Password  string `json:"password"`
	Nickname  string `json:"nickname"`
	Gender    int8   `json:"gender"`
	Phone     string `json:"phone"`
	Avatar    string `json:"avatar"`
	PublicKey string `json:"public_key"`
	CreatedAt int64  `json:"created_at"`
	UpdatedAt int64  `json:"updated_at"`
}

// 数据库连接
func TNewMysqlDB() *DB {
	mysqlDsn := "root:J1ngzha0.com@tcp(127.0.0.1:3306)/db_user"
	return New(Config{Source: mysqlDsn})
}

// 测试链接
func TestNewMysqlDB(t *testing.T) {
	TNewMysqlDB()
	fmt.Println("完成")
}

// 插入语句
func TestDBMysql_Exec(t *testing.T) {
	db := TNewMysqlDB()
	rst, err := db.Exec(
		context.TODO(),
		"insert into t_user(`user_id`,`account`,`password`,`nickname`,`gender`,`phone`,`avatar`,`public_key`,`created_at`,`updated_at`) value (?,?,?,?,?,?,?,?,?,?)",
		100000006,
		"18632620926",
		"占位密码",
		"敬",
		1,
		"18632620926",
		"https://profile.csdnimg.cn/8/F/E/3_weixin_42117918",
		"",
		time.Now().Unix(),
		0,
	)
	if err != nil {
		fmt.Println("执行错误", err.Error())
	} else {
		rowsAffected, _ := rst.RowsAffected()
		lastInsertId, _ := rst.LastInsertId()
		fmt.Println(rowsAffected, lastInsertId)
	}
}

// 更新语句
func TestDBMysql_Exec2(t *testing.T) {
	db := TNewMysqlDB()
	rst, err := db.Exec(
		context.TODO(),
		"update t_user set `gender` = `gender` + 1 where `user_id` = ?",
		100000005,
	)
	if err != nil {
		fmt.Println("执行错误", err.Error())
	} else {
		rowsAffected, _ := rst.RowsAffected()
		lastInsertId, _ := rst.LastInsertId()
		fmt.Println(rowsAffected, lastInsertId)
	}
}

// 删除语句
func TestDBMysql_Exec3(t *testing.T) {
	db := TNewMysqlDB()
	rst, err := db.Exec(
		context.TODO(),
		"delete from t_user where `user_id` = ? limit 1",
		100000005,
	)
	if err != nil {
		fmt.Println("执行错误", err.Error())
	} else {
		rowsAffected, _ := rst.RowsAffected()
		lastInsertId, _ := rst.LastInsertId()
		fmt.Println(rowsAffected, lastInsertId)
	}
}

// 查询结果集
func TestDBMysql_Exec5(t *testing.T) {
	var users []TUser

	db := TNewMysqlDB()
	err := db.Query(
		context.TODO(),
		&users,
		"select * from t_user",
	)
	if err != nil {
		fmt.Println("执行错误", err.Error())
	} else {
		for i, user := range users {
			fmt.Println(i, user, users[i].UserId, users[i].Nickname)
		}
	}
}

// 查询单行
func TestDBMysql_Exec6(t *testing.T) {
	var user TUser

	db := TNewMysqlDB()
	err := db.Get(context.TODO(), &user, "select * from t_user")
	if err != nil {
		fmt.Println("执行错误", err.Error())
	} else {
		fmt.Println(user)
	}
}

type SqlRet struct {
	Total int64 `json:"total"`
}

// 查询特殊语句
func TestDBMysql_Exec7(t *testing.T) {
	var sqlRet SqlRet

	db := TNewMysqlDB()
	err := db.Get(context.TODO(), &sqlRet, "select count(*) as total from t_user")
	if err != nil {
		fmt.Println("执行错误", err.Error())
	} else {
		fmt.Println(sqlRet.Total)
	}
}
