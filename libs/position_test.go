package libs

import (
	"testing"
)

func TestDistance(t *testing.T) {
	type args struct {
		p1 LatLng
		p2 LatLng
	}
	tests := []struct {
		name string
		args args
		want float64
	}{
		{
			name: "得实大厦-清河站-fail",
			args: args{
				p1: LatLng{Lng: 113.4022203113, Lat: 23.1378010917},
				p2: LatLng{Lng: 113.5826193044, Lat: 22.1191433172},
			},
			want: 703.0621282549561,
		},
		{
			name: "得实大厦-清河站-succ",
			args: args{
				p1: LatLng{Lng: 116.320978, Lat: 40.047237},
				p2: LatLng{Lng: 116.314833, Lat: 40.051462},
			},
			want: 703.0621282549561,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := Distance(tt.args.p1, tt.args.p2); got != tt.want {
				t.Errorf("Distance() = %v, want %v", got, tt.want)
			}
		})
	}
}
