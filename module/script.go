package module

import (
	"context"
	"fmt"
	"time"

	"gitee.com/shijingzhao/go-frame/zscript"
)

type ScriptArgHello1 struct {
	A int
}
type ScriptArgHello2 struct {
	A int
}

func ScriptHello1(ctx context.Context, arg any) {
	fmt.Println("ScriptHello1", arg.(ScriptArgHello1).A)
	for {
		if zscript.StopSignal(ctx) {
			return
		}

		fmt.Println(time.Now().Format("2006-01-02 15:04:05.000000"), "ScriptHello1")
		time.Sleep(1 * time.Second)
	}
}

func ScriptHello2(ctx context.Context, arg any) {
	fmt.Println("ScriptHello2", arg.(ScriptArgHello2).A)
	for {
		if zscript.StopSignal(ctx) {
			return
		}

		fmt.Println(time.Now().Format("2006-01-02 15:04:05.000000"), "ScriptHello2")
		time.Sleep(1 * time.Second)
	}
}
