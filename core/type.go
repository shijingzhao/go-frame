package core

type Config struct {
	SerName string `json:"ser_name"` // 项目名称
	Auth    string `json:"auth"`     // 作者
	Pprof   string `json:"pprof"`    // 调试端口
}
