package zscript

import (
	"context"
	"sync"
)

func New(ctx context.Context, config Config) *Server {
	return &Server{
		ctx:      ctx,
		config:   config,
		handlers: make([]Handler, 0),
	}
}

func (ser *Server) AddHandler(handlers []Handler) {
	for _, handler := range handlers {
		ser.handlers = append(ser.handlers, handler)
	}
}

func (ser *Server) Start(wg *sync.WaitGroup) {
	for _, handler := range ser.handlers {
		handler := handler
		wg.Add(1)

		go func() {
			defer wg.Done()
			handler.Function(ser.ctx, handler.Arg)
		}()
	}
}

func StopSignal(ctx context.Context) bool {
	select {
	case <-ctx.Done():
		return true
	default:
		return false
	}
}
