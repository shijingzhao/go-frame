package zredis

import (
	"context"
	"fmt"
	"testing"

	"github.com/redis/go-redis/v9"
)

func TestLibs(t *testing.T) {
	rdb := redis.NewClient(&redis.Options{
		Addr:     "localhost:6379",
		Password: "", // 没有密码，默认值
		DB:       0,  // 默认DB 0
	})
	rs := rdb.SMembers(context.TODO(), "league:opponent:set").Val()
	fmt.Println("rs", rs)
}

func TestNew(t *testing.T) {
	rdb := New(Config{
		Addr:     "localhost:6379",
		Password: "",
		DB:       0,
	})
	rs := rdb.SMembers(context.TODO(), "league:opponent:set").Val()
	fmt.Println("rs", rs)
}
