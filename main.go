package main

import (
	"fmt"
	"time"

	"gitee.com/shijingzhao/go-frame/config"
	"gitee.com/shijingzhao/go-frame/core"
	"gitee.com/shijingzhao/go-frame/module"
	"gitee.com/shijingzhao/go-frame/zcrontab"
	"gitee.com/shijingzhao/go-frame/zhttp"
	"gitee.com/shijingzhao/go-frame/zrpc"
	"gitee.com/shijingzhao/go-frame/zscript"
)

func main() {
	app := core.NewApp().OnLoad(func() {
		// 项目配置
		config.LoadConfig()
	})

	// http
	app.AddHttpRoutes([]zhttp.Route{
		{Method: "POST", Path: "/hello1", Handler: module.Hello1},
	})
	app.AddMiddlewareHttpRoute([]zhttp.Middleware{
		module.MiddlewareHello1,
		module.MiddlewareHello2,
	}, []zhttp.Route{
		{Method: "GET", Path: "/hello2", Handler: module.Hello2},
	})

	// rpc
	app.AddRpcHandler([]zrpc.Handler{
		{Name: "Airth", Obj: new(module.Airth)}, // 提供rpc方法的结构
	})

	// script
	app.AddScriptHandler([]zscript.Handler{
		{Name: "ScriptHello1", UserName: "SJZ", Arg: module.ScriptArgHello1{A: 10}, Env: nil, Function: module.ScriptHello1},
		{Name: "ScriptHello2", UserName: "SJZ", Arg: module.ScriptArgHello2{A: 20}, Env: nil, Function: module.ScriptHello2},
	})

	// crontab
	app.AddCrontabHandler([]zcrontab.Handler{
		{Name: "二进制", Spec: "1,55 20-26/2 12 * * *", Func: func() {
			fmt.Println("二进制", time.Now().Format("2006-01-02 15:04:05.000000"))
		}},
		{Name: "分钟", Spec: "0 */1 * * * * ", Func: func() {
			fmt.Println("分钟", time.Now().Format("2006-01-02 15:04:05.000000"))
		}},
		{Name: "秒", Spec: "*/1 * * * * * ", Func: func() {
			fmt.Println("秒", time.Now().Format("2006-01-02 15:04:05.000000"))
		}},
	})

	app.OnUnload(func() {
		fmt.Println("stop func")
	}).Run()
}
