package zhttp

import (
	"context"
	"encoding/json"
	"fmt"
	"io"
	"net/http"
	"net/url"
	"os"
	"os/signal"
	"strings"
	"sync"
	"syscall"
	"testing"
)

func TestNewServer(t *testing.T) {
	ser := NewServer(context.TODO(), SerConfig{
		Name: "ser1",
		Addr: ":8888",
	})

	ser.AddRoute([]Route{
		{"GET", "/hello1", Hello1},
	})

	ser.AddMiddlewareRoute([]Middleware{
		MiddlewareHello1,
		MiddlewareHello2,
	}, []Route{
		{"GET", "/hello2", Hello2},
	})

	wg := &sync.WaitGroup{}

	//
	wg.Add(1)
	registerExitListening(func() {
		defer wg.Done()
		ser.Close()
	})

	ser.Start(wg)
	fmt.Println("end")

	wg.Wait()
	fmt.Println("wait")
}

func Hello1(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Hello1")
	_, _ = w.Write([]byte("Hello1"))
}
func Hello2(w http.ResponseWriter, r *http.Request) {
	fmt.Println("Hello2")
	_, _ = w.Write([]byte("Hello2"))
}
func MiddlewareHello1(next HandlerFunc) HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("MiddlewareHello1")
		next(w, r)
	}
}
func MiddlewareHello2(next HandlerFunc) HandlerFunc {
	return func(w http.ResponseWriter, r *http.Request) {
		fmt.Println("MiddlewareHello2")
		next(w, r)
	}
}

func registerExitListening(fun func()) {
	ch := make(chan os.Signal, 1)
	signal.Notify(
		ch,
		syscall.SIGTERM,
		syscall.SIGINT,
	)

	go func() {
		_ = <-ch
		fun()
	}()
}

type RS struct {
	Code int    `json:"code"`
	Desc string `json:"desc"`
	Data struct {
		Userid   int         `json:"userid"`
		Nickname string      `json:"nickname"`
		Avatar   string      `json:"avatar"`
		Post     interface{} `json:"post"`
	} `json:"data"`
}

func TestNewClientGet(t *testing.T) {
	rs := &RS{}

	hclient := http.Client{
		Transport:     nil,
		CheckRedirect: nil,
		Jar:           nil,
		Timeout:       0,
	}

	resp, err := hclient.Get("http://php.localhost.com/userinfo.php")
	if err != nil {
		fmt.Println("err", err.Error())
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	body, err := io.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("resp.err", err.Error())
	}

	err = json.Unmarshal(body, &rs)
	if err != nil {
		fmt.Println("json.Unmarshal", err.Error(), string(body))
	}

	fmt.Println("rs", rs.Code, rs.Desc, rs.Data)
}

func TestNewClientPost(t *testing.T) {
	rs := &RS{}

	hclient := http.Client{
		Transport:     nil,
		CheckRedirect: nil,
		Jar:           nil,
		Timeout:       0,
	}

	// url.Value
	// p := url.Values{}
	// p.Set("key", "url.Value")
	// bs, _ := json.Marshal(p)
	// b := strings.NewReader(string(bs))

	// b := strings.NewReader(`{"key":"value"}`)
	b := strings.NewReader(`{"key":"value"}`)
	resp, err := hclient.Post("http://php.localhost.com/userinfo.php", ApplicationJson, b)
	if err != nil {
		fmt.Println("err", err.Error())
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	body, err := io.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("resp.err", err.Error())
	}

	err = json.Unmarshal(body, &rs)
	if err != nil {
		fmt.Println("json.Unmarshal", err.Error(), string(body))
	}

	fmt.Println("rs", rs.Code, rs.Desc, rs.Data.Post, rs.Data)
}

func TestNewClientPostFrom(t *testing.T) {
	rs := &RS{}

	hclient := http.Client{
		Transport:     nil,
		CheckRedirect: nil,
		Jar:           nil,
		Timeout:       0,
	}

	// url.Value
	p := url.Values{}
	p.Set("key", "url.Value")
	resp, err := hclient.PostForm("http://php.localhost.com/userinfo.php", p)
	if err != nil {
		fmt.Println("err", err.Error())
	}
	defer func() {
		_ = resp.Body.Close()
	}()

	body, err := io.ReadAll(resp.Body)
	if resp.StatusCode != 200 {
		fmt.Println("resp.err", err.Error())
	}

	err = json.Unmarshal(body, &rs)
	if err != nil {
		fmt.Println("json.Unmarshal", err.Error(), string(body))
	}

	fmt.Println("rs", rs.Code, rs.Desc, rs.Data.Post, rs.Data)
}
