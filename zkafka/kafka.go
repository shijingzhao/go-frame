package zkafka

import (
	"time"

	"github.com/segmentio/kafka-go"
)

func NewReader() *kafka.Reader {
	return kafka.NewReader(kafka.ReaderConfig{
		Brokers: []string{
			"10.101.166.23:9091",
			"10.101.166.23:9092",
			"10.101.166.23:9093",
		},
		GroupID:        "loc-sjz-test",
		Topic:          "topic-island-lv-chg-msg",
		MinBytes:       1e2,
		MaxBytes:       1e6,
		CommitInterval: time.Millisecond * 100,
		StartOffset:    kafka.LastOffset,
	})
}

func NewWriter() *kafka.Writer {
	return &kafka.Writer{
		Addr: kafka.TCP([]string{
			"10.101.166.23:9091",
			"10.101.166.23:9092",
			"10.101.166.23:9093",
		}...),
		Topic:                  "topic-island-lv-chg-msg",
		Balancer:               nil,
		MaxAttempts:            0,
		WriteBackoffMin:        0,
		WriteBackoffMax:        0,
		BatchSize:              0,
		BatchBytes:             0,
		BatchTimeout:           0,
		ReadTimeout:            0,
		WriteTimeout:           0,
		RequiredAcks:           0,
		Async:                  false,
		Completion:             nil,
		Compression:            0,
		Logger:                 nil,
		ErrorLogger:            nil,
		Transport:              nil,
		AllowAutoTopicCreation: false,
	}
}
